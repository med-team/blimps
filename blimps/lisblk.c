/*=======================================================================
(C) Copyright 1995-2000, Fred Hutchinson Cancer Research Center          
        lisblk.c  reads a .lis file & a .blk file, determines which
	   sequences from .blk are in .lis & writes out a new .lis
	   file with the block sequences flagged to .lsb file in current
	   directory.

	   lisblk <list> <block>
		<list>   	= file containing list of sequences in
				  PROTOMAT format (.lis or .lst file).
                                  Assumes this file has LENGTH= for
                                  each sequence.
		<block>		= block; if "none", looks for a block
				  with same name as <seqs> file.
*/
/*-----------------------------------------------------------------------
   8/1/95   J. Henikoff.   From select.c
   6/25/00  Changed to use blimps routines
========================================================================*/
#define EXTERN
#include <blocksprogs.h>

/*------------------Routines here------------------------------*/
int flag_ids();
int write_lis();
/*----------------- Routines from protomat.c---------------------*/
struct db_id *makedbid();
int get_ids();
struct db_id *check_entry();
struct split_name *split_names();

char Pros[FNAMELEN];
char Title[MAXLINE];

/*======================================================================*/
void main(argc, argv)
int argc;
char *argv[];
{
   FILE *flis, *fblk, *fout;
   char blkfile[FNAMELEN], lisfile[FNAMELEN], outfile[FNAMELEN];
   int lisseq, blkseq, i, done, nout;
   struct db_id *ids;
   struct split_name *lissplit;
   Block *block;

   printf("\nLISBLK: (C) Copyright 1995-2000,");
   printf(" Fred Hutchinson Cancer Research Center");

/*------------- arg 1:  .lis file -------------------------------*/
   if (argc > 1)
      strcpy(lisfile, argv[1]);
   else
   {
      printf("\nEnter name of file containing list of sequences: ");
      gets(lisfile);
   }
   if ( (flis=fopen(lisfile, "r")) == NULL)
   {
      printf("\nCannot open file %s", lisfile);
      exit(-1);
   }
   lissplit = split_names(lisfile);
/*------------- arg 2:  .blk file -------------------------------*/
/*   Look for lisfile.blk then lisfileA.blk     */
   fblk = NULL;  blkfile[0] = '\0';
   if (argc > 2)
      strcpy(blkfile, argv[2]);
   else
   {
      printf("\nEnter name of block file: ");
      gets(blkfile);
   }

   if ((fblk=fopen(blkfile, "r")) == NULL)
   {
      done = NO;  i = strlen(blkfile)-1;
      while (!done && i >=0)
      {
         if (blkfile[i] == '.')
         {  blkfile[i] = '\0'; done=YES; }
         i--;
      }
      strcat(blkfile, "A.blk");
      if ((fblk=fopen(blkfile, "r")) == NULL)
      {
	    printf("\nEnter name of block file: ");
	    gets(blkfile);
      }
   }
   if (fblk == NULL)
      if ((fblk=fopen(blkfile, "r")) == NULL)
      {
	 printf("\nCannot open file %s\n", blkfile);
	 exit(-1);
      }
/*---------------  Get the sequences in the .lis file -----------------*/
   ids = makedbid();
   printf("\nReading %s...", lisfile);
/*-------------  First line of extract file may have a title ----------*/
   fgets(Title, MAXLINE, flis);
   if (Title[0] != '>')
   { rewind(flis); Title[0] = '\0'; }
/*------------Second line may have a directory name --------------*/
   Pros[0] = '\0';
   fgets(Pros, MAXLINE, flis);
   if (strstr(Pros, "/") == NULL)
   {  rewind(flis); Pros[0] = '\0'; }

/*>>>>>>> why isn't ids getting updated here <<<<<<<*/
   lisseq = get_ids(flis, ids);
   printf("\n  %d sequences in %s", lisseq, lisfile);
   fclose(flis);

/*--------------- Compare with sequences in the .blk file -------------*/
   printf ("\nReading %s...", blkfile);
   block = read_a_block(fblk);
   if (block != NULL)
   {
       blkseq = flag_ids(block, ids);
       printf(" %d sequences from %s\n  were found in %s\n",
	   blkseq, lisfile, blkfile);
   }
   else
   {
      printf("No block found in %s\n", blkfile);
   }
   fclose(fblk);


/*--------------- Open the new output list file -----------------------*/
/*-----------  Make the .lsb file in the current directory----------*/
   outfile[0] = '\0';
   strncat(outfile, lisfile+lissplit->dir_len, lissplit->name_len);
   outfile[lissplit->name_len] = '\0';
   strcat(outfile, ".lsb");
   if ( (fout=fopen(outfile, "w+t")) == NULL)
   {
      printf("\nCannot open file %s\n", outfile);
      exit(-1);
   }
   if (strlen(Title)) fprintf(fout, "%s", Title);
   if (strlen(Pros))  fprintf(fout, "%s", Pros);
   nout = write_lis(fout, ids);
   printf("%d sequences written to %s\n", nout, outfile);

   fclose(fout);

   printf("\n");
   exit(0);
}  /*  end of main */
/*================================================================*/
int flag_ids(block, ids)
Block *block;
struct db_id *ids;
{
   struct db_id *did;
   int s, nfound;

   nfound = 0;
   for (s=0; s < block->num_sequences; s++)
   {
      did = check_entry(ids, block->sequences[s].name);
      if (did != NULL) did->block = YES;
      nfound++;
   }
   return(nfound);
} /* end of flag_ids */
/*========================================================================
==========================================================================*/
int write_lis(flis, ids)
FILE *flis;
struct db_id *ids;
{
   struct db_id *id;
   int nlis;

   nlis = 0;
   id = ids->next;
   while (id != NULL)
   {
       nlis += 1;
       fprintf(flis, "%-12s", id->entry);
       if (strlen(id->ps)) fprintf(flis, "  PS=%s", id->ps);
       if (id->len > 0 ) fprintf(flis, "  LENGTH=%-6d", id->len);
       if (id->frag)     fprintf(flis, "  FRAGMENT");
       if (id->block)    fprintf(flis, "  BLOCK");
       if (id->lst)      fprintf(flis, "  LST");
       fprintf(flis, "\n");
       id = id->next;
   }
   return(nlis);
}  /* end of write_lis */
