/* 
	blklis.c:	Read block & make list of sequences for uextract
		blklis <block input file> <list output file>
*/
#define EXTERN

#include "blocksprogs.h"

/*---------------------- Global variables ----------------------------*/
char Version[12] = " 8/24/99.1";		/* Version date */

/*=======================================================================*/
/*
 * main
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE  *fin, *fout;
  char infile[80], outfile[80], ac[30], id[80], *ptr;
  int s;
  Block *block;

  ErrorLevelReport = 2;		/* suppress BLIMPS errors */

   printf("\nblklis: (C) Copyright 1999");
   printf(" Fred Hutchinson Cancer Research Center\n");

   /*  open files */
   if (argc > 1)
      strcpy(infile, argv[1]);
   else
   {
      printf("\nEnter name of file of blocks: ");
      gets(infile);
   }
   if ( (fin=fopen(infile, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", infile);
      exit(-1);
   }
   if (argc > 2)
      strcpy(outfile, argv[2]);
   else
   {
      printf("\nEnter name of output file: ");
      gets(outfile);
   }
   if ( (fout=fopen(outfile, "w+")) == NULL)
   {
      printf("\nCannot open file %s\n", outfile);
      exit(-1);
   }

  /*  Process first block only */
  block = read_a_block(fin);
  if (block != NULL)
  {
     strcpy(ac, block->number); ac[7] = '\0';
     strcpy(id, block->id); ptr = strtok(id, ";");
     fprintf(fout, ">%s ;%s;%s; ; $\n", 
	ac, id, block->de);
     fprintf(fout, "pros/\n");
     for (s=0; s<block->num_sequences; s++)
	fprintf(fout, "%s\n", block->sequences[s].name);
  }

  fclose(fin); fclose(fout);

  exit(0);
} /*  end of main */
