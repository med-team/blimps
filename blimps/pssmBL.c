/*   pssmBL.c   Read blocks db & pssmdist.dat file, insert
                99.5%= and strength= on BL line
		pssmBL <pssmdist.dat> <blocks.in> <blocks.out>
--------------------------------------------------------------------
 4/ 8/95 J. Henikoff 
 4/12/95 Open blocks.out for append instead of write.
10/ 6/99 Changes for Blimps 3.3
====================================================================*/

#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

void get_info();

/*=======================================================================*/
int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp, *pfp;
  Block *block;
  char bdbname[MAXNAME], conname[MAXNAME], pssmname[MAXNAME];

/* ------------1st arg = pssmdist.dat file------------------------------*/
   if (argc > 1)
      strcpy(pssmname, argv[1]);
   else
   {
      printf("\nEnter name of pssmdist.dat file: ");
      gets(pssmname);
   }
   if ( (pfp=fopen(pssmname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", pssmname);
      exit(-1);
   }
/* ------------2st arg = blocks database -------------------------------*/
   if (argc > 2)
      strcpy(bdbname, argv[2]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }
/* ------------3nd arg = new blocks database ---------------------*/
   if (argc > 3)
      strcpy(conname, argv[3]);
   else
   {
      printf("\nEnter name of new blocks database: ");
      gets(conname);
   }
   if ( (ofp=fopen(conname, "a")) == NULL)
   {
      printf("\nCannot open file %s\n", conname);
      exit(-1);
   }

/*-----------------------------------------------------------------*/

  while ((block = read_a_block(bfp)) != NULL)
  {
     get_info(pfp, block);
     output_block(block, ofp);
     free_block(block);
  }
   
  fclose(bfp); fclose(ofp); fclose(pfp);
  exit(0);

}  /* end of main */
/*=======================================================================
	Get 99.5% & strength from pssmdist.dat file & replace any
	values already in block
========================================================================*/
void get_info(pfp, block)
FILE *pfp;
Block *block;
{
   char line[MAXNAME], *ptr;

   if (fgets(line, MAXNAME, pfp) != NULL)
   {
      ptr = strtok(line, " \t\r\n");		/* AC */
      if (ptr != NULL && strcmp(ptr, block->number) == 0)
      {
         ptr = strtok(NULL, " \t\r\n");		/* 99.5% score */
         if (ptr != NULL)
         {
            block->percentile = atoi(ptr);
            ptr = strtok(NULL, " \t\r\n");	/* Median TP score */
            if (ptr != NULL)
            {
               ptr = strtok(NULL, " \t\r\n");	/* Strength */
               if (ptr != NULL) block->strength = atoi(ptr);
	       if (block->strength < 0) block->strength = 0;  /*pssmdist bug?*/
            }
         }
      }
      /*   Now rewrite the BL line  */
      strcpy(line, block->bl);
      ptr = strtok(line, "; \t\r\n");		/* motif or other info */
      sprintf(block->bl, "%s; width=%d; seqs=%d; 99.5%%=%d; strength=%d;",
	ptr, block->width, block->num_sequences, 
	block->percentile, block->strength);
   }
   
}  /* end of get_info */
