/*   COPYRIGHT 1999-2003 Fred Hutchinson Cancer Research Center
		show_aligned_blocks.c
      This program will print to the screen two specified blocks offset
      to show their alignment. Executed by LAMA_alignment.sh

6/20/99 Changed for longer sequence names (18 chars)
12/23/06 Changed for longer sequence names (20 chars)

====================================================================*/

#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#define YES 1
#define NO 0

#include <blocksprogs.h>


/*
 * Local variables and data structures
 */

void   fprint_matrix() ;

/*=======================================================================*/
/*
 * main
 *   controls flow of program
 *   Parameters: argc, argv
 *   Error codes:
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE  *bfp1, *bfp2, *outf;
  Block *block1, *block2;
  char  bdbname1[MAXNAME], bdbname2[MAXNAME];
  char  blkname1[SMALL_BUFF_LENGTH], blkname2[SMALL_BUFF_LENGTH];
  char  offset[MAXNAME] ;
  int   start1, start2, alignment_length, i1, i2 ;

   if (argc != 8)
      {
      printf("%s  blocks_file  block1_accession  block1_alignment_start  blocks_file  block2_accession  block2_alignment_start  alignment_length\n", 
             argv[0]) ;
      exit(-1) ;
      }


/* ------------1st arg = blocks database -------------------------------*/
   strcpy(bdbname1, argv[1]);

   if ( (bfp1=fopen(bdbname1, "r")) == NULL)
      {
      fprintf(stderr,"\nCannot open file %s\n", bdbname1);
      exit(-2);
      }

/* ------------2nd arg = block1 accession -------------------------------*/
   strcpy(blkname1, argv[2]);

/* ------------4th arg = alignment start -------------------------------*/
   start1 = atoi(argv[3]) ;

   if (start1 < 1)
      {
      fprintf(stderr,"\nThird argument should be the start position of the aligned region in block 1\n");
      exit(-3);
      }

/* ------------4th arg = blocks database -------------------------------*/
   strcpy(bdbname2, argv[4]);

   if ( (bfp2=fopen(bdbname2, "r")) == NULL)
      {
      fprintf(stderr,"\nCannot open file %s\n", bdbname2);
      exit(-2);
      }

/* ------------5th arg = block2 accession -------------------------------*/
   strcpy(blkname2, argv[5]);


/* ------------6th arg = alignment start -------------------------------*/
   start2 = atoi(argv[6]) ;

   if (start2 < 1)
      {
      fprintf(stderr,"\nSixth argument should be the start position of the aligned region in block 2\n");
      exit(-3);
      }


/* ------------7th arg = alignment length -------------------------------*/
   alignment_length = atoi(argv[7]) ;

   if (alignment_length < 1)
      {
      fprintf(stderr,"\nSeventh argument should be the alignment length\n");
      exit(-3);
      }


/* output to screen */
   outf = stdout ;

/* get blocks */

   while ((block1 = read_a_block(bfp1)) != NULL && 
         (strcmp(block1->number,blkname1) != 0))  free_block(block1) ;

   if (block1 == NULL)
      {
      fprintf(stderr,"\nBlock %s not found in file %s\n", blkname1, bdbname1);
      exit(-4);
      }

   while ((block2 = read_a_block(bfp2)) != NULL && 
         (strcmp(block2->number,blkname2) != 0))  free_block(block2) ;

   if (block2 == NULL)
      {
      fprintf(stderr,"\nBlock %s not found in file %s\n", blkname2, bdbname2);
      exit(-4);
      }

/* consistency checks */

   if (start1 + alignment_length - 1 > block1->width ||
       start2 + alignment_length - 1 > block2->width ||
       start1 > block1->width ||
       start2 > block2->width)
      {
      fprintf(outf, "Something seems wrong in the alignment coordinates !!!\n\n") ;
      exit(-5) ;
      }

/* write block 1 */

   fprintf(outf, "%s\n", block1->number) ;
   for (i1=0; i1<block1->num_sequences; i1++) 
      {
      offset[0] = '\0' ;
      for (i2=0; i2 < (start2-start1); i2++) strcat(offset, " ") ;

      fprintf(outf, "%-20.20s (%5d) %s",
              block1->sequences[i1].name, block1->sequences[i1].position,
              offset) ;

      for (i2=0; i2<block1->sequences[i1].length; i2++) 
          {
	  fprintf(outf, "%c",
                  aa_btoa[block1->sequences[i1].sequence[i2]]);
	  }
      fprintf(outf, "\n") ;
      }

/* write alignment line */
   fprintf(outf, "\n                   aligned:") ;
   for(i1=0; i1<max(start1,start2)-1; i1++) fprintf(outf, " ") ;
   for(i1=0; i1<alignment_length; i1++) fprintf(outf, "=") ;
   fprintf(outf, "\n") ;

/* write block 2 */

   fprintf(outf, "%s\n", block2->number) ;
   for (i1=0; i1<block2->num_sequences; i1++) 
      {
      offset[0] = '\0' ;
      for (i2=0; i2 < (start1-start2); i2++) strcat(offset, " ") ;

      fprintf(outf, "%-20.20s (%5d) %s",
              block2->sequences[i1].name, block2->sequences[i1].position,
              offset) ;

      for (i2=0; i2<block2->sequences[i1].length; i2++) 
          {
	  fprintf(outf, "%c",
                  aa_btoa[block2->sequences[i1].sequence[i2]]);
	  }
      fprintf(outf, "\n") ;
      }

  free_block(block1) ;
  free_block(block2) ;

  fclose(bfp1);
  fclose(bfp2);

  exit(0);

}  /* end of main */

