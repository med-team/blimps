/*  COPYRIGHT 1997-1999 Fred Hutchinson Cancer Research Center
    retblock.c  Extract some or all blocks from a blocks database to
		  separate out files or to stdout
           retblock <input blocks file> <AC|all> [-n] [-f]
	   Creates files named AC.blk
--------------------------------------------------------------------
 1/10/97 J. Henikoff (from extblock.c & blexplode.c)
 4/14/97 Added floating point output options.
 7/31/97 Compares shorter of input AC and block AC.
 1/23/99 exit(-2) if requested block wasn't found
 6/14/99 Adjustments for longer sequence names
====================================================================*/

#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

/*=======================================================================*/
/*
 * main
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
   FILE *bfp, *ofp;
   Block *block;
   char bdbname[MAXNAME], outname[MAXNAME], acname[MAXNAME];
   int outfloat, outflag, allflag, aclen, okay, nout;

   if (argc < 2)
   {
      printf("RETBLOCK: Copyright 1997 Fred Hutchinson Cancer Research ");
      printf("Center\n");
      printf("Extract blocks from a Blocks Database\n");
      printf("USAGE:  retblock <input blocks> <AC|all> [[-n] -f]\n");
      printf("          <input blocks> = file of blocks\n");
      printf("          <AC|all> => extract AC or all blocks\n");
      printf("          -n       => output to stdout, otherwise each block\n");
      printf("                      will be written to AC?.blk\n");
      printf("		-f	=> floating point output\n");
   }

   allflag = outflag = FALSE;
/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }

/* ------------2nd arg = AC --------------------------------------------*/
   allflag = FALSE;
   if (argc > 2)
      strcpy(acname, argv[2]);
   else
   {
      strcpy(acname, "all");
      printf("\nEnter AC of blocks to extract, or all [%s]: ", acname);
      gets(acname);
   }
   if (strcasecmp(acname, "all") == 0) allflag = TRUE;

   /* ------------3rd & 4th optional arg = stdout flag/floating point-------*/
   outflag = FALSE;
   outfloat = FALSE;
   if (argc > 3 && strcasecmp(argv[3], "-n") == 0) outflag = TRUE;
   if (argc > 3 && strcasecmp(argv[3], "-f") == 0) outfloat = TRUE;
   if (argc > 4 && strcasecmp(argv[4], "-n") == 0) outflag = TRUE;
   if (argc > 4 && strcasecmp(argv[4], "-f") == 0) outfloat = TRUE;

   /*-----------------------------------------------------------------*/

   nout = 0;
   while ((block = read_a_block(bfp)) != NULL)
   {
     okay = FALSE;
     aclen = strlen(acname);
     /* If they type BL00031A, but there's only a single block, BL00031,
        then be sure to return that */
     if (strlen(block->number) < aclen) aclen = strlen(block->number);
     if (allflag || strncasecmp(acname, block->number, aclen) == 0)
        okay = TRUE;
     if (okay)
     { 
        if (!outflag)
        {
           strcpy(outname, block->number);
           strcat(outname, ".blk");
           if ( (ofp=fopen(outname, "w")) == NULL)
           {
               printf("\nCannot open file %s\n", outname);
               exit(-1);
           }
        }
        else ofp = stdout;
        if (outfloat) output_block_s(block, ofp, FLOAT_OUTPUT);
        else          output_block_s(block, ofp, INT_OUTPUT);
        if (!outflag) fclose(ofp);
        nout++;
      }
      free_block(block);
   }
   
   fclose(bfp);
   if (nout > 0) exit(0);
   else          exit(-2);

}  /* end of main */
