/*=====================================================================
(C) Copyright 1997-2000, Fred Hutchinson Cancer Research Center           
	coduse.c: Read a CUTG codon usage file & make a BLIMPS
		codon.use file for an organism

CUTG .spsum file entry format:

organism: number of compiled CDS
CGA CGC CGG CGU AGA AGG CUA CUC CUG CUU UUA UUG UCA UCC UCG UCU AGC AGU ACA ACC ACG ACU CCA CCC CCG CCU GCA GCC GCG GCU GGA GGC GGG GGU GUA GUC GUG GUU AAA AAG AAC AAU CAA CAG CAC CAU GAA GAG GAC GAU UAC UAU UGC UGU UUC UUU AUA AUC AUU AUG UGG UAA UAG UGA
-----------------------------------------------------------------------
 2/12/97 J. Henikoff
=======================================================================*/

#define EXTERN				/*  Needed for Blimps routines */
#define MAXNAME 80

#include <blocksprogs.h>

double read_cutg();


/*   Blimps2Gcode[Blimps order (ACGU)] = Gcode order (UCAG)                */
int Blimps2Gcode[64] = {
 42, 41, 43, 40,  38, 37, 39, 36,  46, 45, 47, 44,  34, 33, 35, 32,     /*A*/
 26, 28, 27, 24,  22, 21, 23, 20,  30, 29, 31, 28,  18, 17, 19, 16,     /*C*/
 58, 57, 59, 56,  54, 53, 55, 52,  62, 61, 63, 60,  50, 49, 51, 48,     /*G*/
 10,  9, 11,  8,   6,  5,  7,  4,  14, 13, 15, 12,   2,  1,  3,  0  };  /*U*/

/*   Blimps2CUTG[Blimps order] = CUTG order                     */
int Blimps2CUTG[64] = {
 38, 40, 39, 41,  18, 19, 20, 21,   4, 16,  5, 17,  56, 57, 59, 58,     /*A*/
 42, 44, 43, 45,  22, 23, 24, 25,   0,  1,  2,  3,   6,  7,  8,  9,     /*C*/
 46, 48, 47, 49,  26, 27, 28, 29,  30, 31, 32, 33,  34, 35, 36, 37,     /*G*/
 61, 50, 62, 51,  12, 13, 14, 15,  63, 52, 60, 53,  10, 54, 11, 55  };  /*U*/

/*   CUTG2Blimps[CUTG order] = Blimps order                     */
int CUTG2Blimps[64] = {
 24, 25, 26, 27,   8, 10, 28, 29,  30, 31, 60, 62,  52, 53, 54, 55,
  9, 11,  4,  5,   6,  7, 20, 21,  22, 23, 36, 37,  38, 39, 40, 41,
 42, 43, 44, 45,  46, 47,  0,  2,   1,  3, 16, 18,  17, 19, 32, 34,
 33, 35, 49, 51,  57, 59, 61, 63,  12, 13, 15, 14,  58, 48, 50, 56  };  /*U*/

/*  Codons in Blimps order  */
char Codons[64][3] = {
"AAA","AAC","AAG","AAU", "ACA","ACC","ACG","ACU", "AGA","AGC","AGG","AGU",
"AUA","AUC","AUG","AUU", "CAA","CAC","CAG","CAU", "CCA","CCC","CCG","CCU",
"CGA","CGC","CGG","CGU", "CUA","CUC","CUG","CUU", "GAA","GAC","GAG","GAU",
"GCA","GCC","GCG","GCU", "GGA","GGC","GGG","GGU", "GUA","GUC","GUG","GUU",
"UAA","UAC","UAG","UAU", "UCA","UCC","UCG","UCU", "UGA","UGC","UGG","UGU",
"UUA","UUC","UUG","UUU" };

/*======================================================================*/
int main(argc, argv)
int argc;
char *argv[];
{
   FILE *fin, *fout;
   char infile[MAXNAME], outfile[MAXNAME], organism[MAXNAME], line[MAXLINE];
   int first, done;
   double tot_codons;

   printf("CODUSE: (C) Copyright 1997");
   printf(" Fred Hutchinson Cancer Research Center\n");
   if (argc > 1) strcpy(infile, argv[1]);
   else
   {
      printf("\nEnter name of CUTG spsum file: ");
      gets(infile);
   }
   if ( (fin=fopen(infile, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", infile);
      exit(-1);
   }

   first = TRUE;
   if (argc > 2)  strcpy(organism, argv[2]);
   else
   {
      printf("Enter organism name (leave blank for first organism in file): ");
      gets(organism);
   }
   if (strlen(organism))
   {
      printf("Looking for organism %s in %s\n", organism, infile);
      first = FALSE;
   }
   else
   {
      printf("Taking first organism in %s", infile);
      first = TRUE;
   }

   if (argc > 3) strcpy(outfile, argv[3]);
   else
   {
      printf("\nEnter name of Blimps codon usage output file: ");
      gets(outfile);
   }
   if ( (fout=fopen(outfile, "w")) == NULL)
   {
      printf("\nCannot open file %s\n", outfile);
      exit(-1);
   }

/*---------------------------------------------------------------------*/
   done = FALSE;
   while(!done && fgets(line, MAXLINE, fin) != NULL)
   {
      if (first ||
          (strncasecmp(line, organism, strlen(organism)) == 0) )
      {
         strcpy(organism, line);
         tot_codons = read_cutg(fin, fout);
         done = TRUE;
      }
   }
/*---------------------------------------------------------------------*/
   fprintf(fout, "---------------------------------------------\n");
   fprintf(fout, "%s\n", outfile);
   fprintf(fout, "Codon usage for %s from %s:", organism, infile);
   fprintf(fout, " %.0f codons\n", tot_codons);
   fclose(fin);  fclose(fout);
   exit(0);
}  /*  end of main */
/*=========================================================================*/
double read_cutg(fin, fout)
FILE *fin, *fout;
{
   char line[MAXLINE], *ptr, ctemp[6];
   double cutg[64], total;
   int i, b;

   total = 0.0;
   if (fgets(line, MAXLINE, fin) != NULL)
   {
      ptr = strtok(line, " \t\r\n");
      i = 0;
      while (i < 64) 
      {
         if (ptr != NULL)
         {
            cutg[i] = atof(ptr); 
	    total += cutg[i++];
            ptr = strtok(NULL, " \t\r\n");
         }
         else cutg[i++] = 0.0;
      }

      for (i=0; i< 64; i++)
      {
         b = Blimps2CUTG[i];
         strncpy(ctemp, Codons[i], 3); ctemp[3] = '\0';
/*         fprintf(fout, "%12.4f	-- [%2d] %s\n", cutg[b], i, ctemp); */
         fprintf(fout, "%8.6f	-- [%2d] %s\n", cutg[b]/total, i, ctemp);
      }
   }
/*   printf("%f\n", total); */
   return(total);
}   /*  end of read_cutg */

/*======================================================================*/
