=head1 NAME

fastaseqs - converts a file of sequences to FASTA format

=head1 SYNOPSIS

fastaseqs <INPUTFILE> <OUTPUTFILE>

=head1 DESCRIPTION

fastaseqs converts a file of sequences to FASTA format.

=head1 AUTHOR

J. Henikoff

=head1 COPYRIGHT AND LICENSE

Copyright 1998, Fred Hutchinson Cancer Research Center

Please refer to /usr/share/doc/*blimps*/copyright for the license.

=head1 SEE ALSO

L<http://blocks.fhcrc.org/blocks/uploads/blimps/>
