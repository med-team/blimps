#!/bin/csh 
#    This is multimat.csh
#	Combine independent blimps search results for blocks
#	from the same family vs a sequence database
#
unalias rm
setenv BLIMPS_DIR ../..
#	Block family name; 1st 7 characters of each AC
#	 must be the same for each query block
set name = example
#	Blocks database for alignments;
#	  concatenate all of the query blocks into this file
set dat = $name.blocks
rm $dat $name?.blk.bli

#	Target database of sequences to search
set db = example.pros

#	Search each block vs sequence database with blimps
#	Blimps results are in $name*.bli files
set blks = ($name?.blk)
foreach blk ($blks)
	echo "BLock  			$blk" > $name.cs
	echo "DBase			$db" >> $name.cs
	echo "OUtput_file		$blk.bli" >> $name.cs
	echo "REpeats_allowed		NO" >> $name.cs
	echo "NUmber_to_report	0" >> $name.cs
	echo "//" >> $name.cs

	$BLIMPS_DIR/bin/blimps $name.cs

	cat $blk >> $dat
end

#	Post-process blimps results with multimat
#	Number of hits to report
set hits = 1000
#     The sequences in this list are considered "true positives"
#	and will not be reported by multimat (optional)
set list = none
#      Output file - frequency distribution of scores
set out = multimat.out
#
#	Execute multimat excluding known true positive hits
#$BLIMPS_DIR/bin/multimat $hits $dat $list $name?.blk.bli >$out
#	Use none instead of $list to include known true positive hits
$BLIMPS_DIR/bin/multimat $hits $dat none $name?.blk.bli >$out
exit(0)
