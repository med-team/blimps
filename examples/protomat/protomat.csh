#!/bin/csh

#    protomat.csh <file_of_protein_sequences_in_fasta_format>
#	Output files written with same prefix as input file:
#		.out	blocks in "pretty" format
#		.mblks	blocks 
#		.mcob	cobbler sequence
#		.mmap	block map
#		.mmast	PSSM for mast

setenv BLIMPS_DIR ../..
set bindir = ../../bin
set path = ($bindir $path)
#       SWISS 38 sequences & residues for calibrate step
set NSEQ = 80000
set NRES = 29085965
unalias rm
unalias mv
unalias cp
limit coredumpsize 1k
limit datasize 32m

set pros = $1
set r = $pros:r
set frq = $BLIMPS_DIR/docs/default.amino.frq

#  makelis reads $prefix.in and writes $prefix.lis, .pros, .seqlen, .err, .warn
#$bindir/makelis $prefix $return

#	Find out how many sequences there are
set nseq = (`grep -c '^>' $pros`)
if ( $nseq > 250 ) then
   echo -n "ERROR: Your input has $nseq sequences which" >> $r.out
   echo " exceed's Block Maker's limit of 250 $nseq sequences" >> $r.out
   exit(-1)
endif

#	Make blocks from the proteins (motifj executes motomat)
#	NOTE: motifj writes $r.motifj.pros with MINIMUM seq len marked for gibbs
echo "              **BLOCKS from MOTIF**" >> $r.out
echo " " >> $r.out
#	For dups:  motifj 4 -$pros 0 dups 17 (0 seqs => n/2 to start)
$bindir/motifj 4 -$r.pros >& /dev/null
#  Run motomat again so sequences are NOT clumped (users want original order)
$bindir/motomat $r.mot 1 1 -10 >& /dev/null

if ( -e $r.blks ) then
   # Calibrate blocks for blimps searching
   #	Compute searching PSSMs
   $bindir/blk2pssm $r.blks $r.mats B 3 >& /dev/null
   #	Compute frequency PSSMs for true positive distribution
   $bindir/blk2pssm $r.blks $r.obsf B 21 >& /dev/null
   #    Calculate theoretical score distribution, writes pssmdist.dat
   $bindir/pssmdist $r.mats $r.obsf $frq $NSEQ $NRES >& /dev/null
   #    Insert blimps scoring info in blocks on BL line
   $bindir/pssmBL pssmdist.dat $r.blks $r.cblks >& /dev/null
   rm pssmdist.dat $r.mats $r.obsf >& /dev/null

   # Add position-specific sequence weights to blocks
   $bindir/blweight $r.cblks $r.mblks P M >& /dev/null
   #	Produce a "multiple alignment"
   $bindir/blalign $r.mblks >> $r.out
   #	Make cobbler sequence = $r.mcob
   echo "TY	2"	     > $r.cf
   echo "BL	$r.mblks"   >> $r.cf
   echo "DB	$r.pros"    >> $r.cf
   echo "OU	$r.mcob"    >> $r.cf
   echo "SU	$BLIMPS_DIR/docs/default.iij" >> $r.cf
   $bindir/cobbler $r.cf >& /dev/null
   #		Show the cobbler sequence now
   echo "" >> $r.out
   echo "" >> $r.out
   echo "      **COBBLER sequence from MOTIF**" >> $r.out
   cat $r.mcob | tr '\015' ' ' >> $r.out
else
   echo "ERROR: No blocks produced by MOTIF" >> $r.out
endif
#
#
#
#---------------------------------------------------------------------------
#	Format PSSMs for MAST searches & make map files
if (-e $r.mblks) then
   $bindir/blk2pssm $r.mblks $r.mmast M >& /dev/null
   #	Make block map files
   $bindir/makeblockmap $r.mblks $r.mmap >& /dev/null
endif

#---------------------------------------------------------------------------

#   	Clean up
rm motomat.err $r.mot $r.blks $r.cblks $r.cf $r.motifj.pros >& /dev/null
exit(0)
