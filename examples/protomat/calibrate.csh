#!/bin/csh
#		Theoretical calibration
#		Create blks_final/ first
#
unalias rm
unalias mv
unalias cp

set docs = /howard/servers/blimps/blimps-3.5/docs
set bin = /howard/servers/blimps/blimps-3.5/bin
#	Use a .frq where the 20aas add up to exactly 1.00000
set frq = $docs/default.amino.frq

#	SWISS 38 sequences & residues
set NSEQ = 80000
set NRES = 29085965

rm pssmdist.dat
set ps = (process.PRa process.PRb bad.PR)
foreach p ($ps)
 set entries = (`awk '{ print $1 }' $p`)
 foreach ipb ($entries)

   set blks = blks_add/$ipb.blks
   set new = blks_final/$ipb.blks
   set mats = $ipb.mats
   set obsf = $ipb.obsf

   #       Make matrices and observed frequency files:
   $bin/blk2pssm $blks $mats B 3
   $bin/blk2pssm $blks $obsf B 21
#
#		writes pssmdist.dat
   $bin/pssmdist $mats $obsf $frq $NSEQ $NRES >& /dev/null
   $bin/pssmBL pssmdist.dat $blks $ipb.temp
   cat pssmdist.dat >> pssmdist.$p
   rm pssmdist.dat $ipb.cf $mats $obsf >& /dev/null

#	800seqs is made by addseqs.pl
   set flag = `grep -c $ipb 800seqs`
   if (flag == 0) then
      $bin/blweight $ipb.temp $ipb.c80 C80 M >& /dev/null
      $bin/blweight $ipb.c80 $new P M >& /dev/null
      rm $ipb.c80
   else
      $bin/blweight $ipb.temp $new P M >& /dev/null
   endif
   rm $ipb.temp

 end
end

exit(0)
