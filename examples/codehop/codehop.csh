#!/bin/csh
#      codehop.csh <blocks> <out>
#
#  Make codehops from a set of blocks
#
unalias rm
unalias mv
#echo "codehop.csh"
#echo $1
#echo $2
#echo $3
#
#
setenv BLIMPS_DIR ../..
set bin = $BLIMPS_DIR/bin
#	See coduse.csh to create the codon usage files
set docs = $BLIMPS_DIR/docs
#
#	Type "$bin/codehop" to see parameter options
if ( -e $1 ) then
#	These are the WWW defaults
#  $bin/codehop $1 -O3 >& $2
#	Use an alternative codon usage file
   $bin/codehop $1 -C$docs/arab.codon.use -O3 >& $2
#	Use most common codons (many people prefer this option)
#  $bin/codehop $1 -O3 -M >& $2
#  
else
   echo "Cannot open blocks file $1."
endif
#

set files = (distribution/*.*)
foreach file ($files)
	set test = $file:t
echo "Checking $test..."
	diff $test $file
echo ""
end
exit(0)
