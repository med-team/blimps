#!/bin/csh
#
set ins = (*.in)
foreach in ($ins)
        ./process.csh $in
        echo "Checking $in..."
        diff $in.wblks distribution/$in.wblks
end

exit
