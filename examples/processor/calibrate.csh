#!/bin/csh
#	Calibrate blocks for searching with blimps

#		calibrate.csh blks-in blks-out
#
set BLIMPS_DIR = ../../
set bin = $BLIMPS_DIR/bin
set frq = $BLIMPS_DIR/docs/default.amino.frq

unalias rm
unalias mv

#	Parameters for theoretical calibration
set NSEQ = 80000
set NRES = 29085965

#       Make matrices and observed frequency files
$bin/blk2pssm $1 $1.mats B 3 >& /dev/null
$bin/blk2pssm $1 $1.obsf B 21 >& /dev/null
#               writes $1.dat
$bin/pssmdist $1.mats $1.obsf $frq $NSEQ $NRES $1 >& /dev/null
$bin/pssmBL $1.dat $1 $2 >& /dev/null

rm $1.dat $1.mats $1.obsf

exit(0)
